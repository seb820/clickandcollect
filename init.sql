drop database  if exists music; 
create database music;
use music;

create table client(
    id int not null auto_increment primary key,
    nom varchar(255) not null,
    email varchar(255) not null,
    tel varchar(255) not null
);
 

create table commande(
    id int auto_increment primary key,
    id_client int,
    etat enum('panier', 'validée', 'prete', 'collectée') default 'panier'
);
create table product (
    id int auto_increment primary key,
    nom varchar(255) not null,
    img varchar(2048) not null,
    price float,
    file_path varchar(255) not null,
    available boolean 
);

create table produit_commande(
    id_commande int,
    id_produit int,
    qt float,
    primary key (id_commande, id_produit)
);


DROP USER  if exists 'admin'@'127.0.0.1';
CREATE USER 'admin'@'127.0.0.1' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON music.* TO 'admin'@'127.0.0.1';

