<title>HOMEPAGE</title>
<link rel = "stylesheet"  href="style.css"/>
<header class="homepage-header">
<nav>
     <h3>BOUTIQUE</h3>
     <img class="logo"src="" alt="">
        <ul class="list">
         <li><a href="adminSignIn.php">LOGIN</a></li>
         <li class="cart"> <a href="panier.php"><ion-icon name = "basket"> </ion-icon><span> Cart </span></a></li>
        </ul>
</nav>
</header>
<?php 
include_once "pdo.php";
$images = readAll();
?>
<div class="container">
<?php
for ($i=0;$i<count($images);$i++) { ?>
    <h2 class="name"><?= $images[$i]["nom"] ?></h2>
    <img class="inner-images" src="<?=$images[$i]["img"] ?>">
    <p class="price"><?= $images[$i]["price"] ?><span>$</span></p>
    <audio class="audio"
    controls 
    src="<?=$images[$i]["file_path"]?>">
    <a href="<?=$images[$i]["file_path"]?>"></a>
    </audio>
    <?= $images[$i]["available"] ?>
    
    <form action="panier.php" method="POST">
        <input type="hidden" name = "id" value="<?=$images[$i]["id"]?>">
        <input type="hidden" name="nom" value="<?=$images[$i]["nom"]?>">
        <input type="hidden" name="img" value="<?=$images[$i]["img"]?>">
        <input type="hidden" name="price" value="<?=$images[$i]["price"]?>">
        <input type="hidden" name="file_path" value="<?=$images[$i]["file_path"]?>">
        <input type="hidden" name="available" value="<?=$images[$i]["available"]?>">
<input type="submit" id="btn" name="btn" value="add to cart">
    </form>
</div>
<?php 
}
?>
 <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
</body>
</html>